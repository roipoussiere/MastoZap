const platformVersion = require('zapier-platform-core').version;
const authentication = require('./authentication');
const appVersion = require('./package.json').version;

// To include the Authorization header on all outbound requests, simply define a function here.
// It runs before each request is sent out, allowing you to make tweaks to the request in a
// centralized spot
const includeBearerToken = (request, z, bundle) => {
  if (bundle.authData.access_token) {
    request.headers.Authorization = `Bearer ${bundle.authData.access_token}`;
  }
  return request;
};

const App = {
  // This is just shorthand to reference the installed dependencies you have. Zapier will
  // need to know these before we can upload
  version: appVersion,
  platformVersion,

  authentication,

  beforeRequest: [
    includeBearerToken,
  ],

  afterResponse: [
  ],

  resources: {
  },

  // If you want your trigger to show up, you better include it here!
  triggers: {
  },

  // If you want your searches to show up, you better include it here!
  searches: {
  },

  // If you want your creates to show up, you better include it here!
  creates: {
  },
};

// Finally, export the app.
module.exports = App;
