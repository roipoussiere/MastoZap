# Contributing

Hey, welcome to the party! 🎉

Thank you so much to contribute to MastoZap. 😘

## Asking questions, suggesting wonderful ideas or reporting bugs

You can [submit an issue️](https://framagit.org/roipoussiere/MastoZap/issues/new) on this GitLab repository.

If you are not familiar with the GitLab issue tracker, you can also:

- ️🐘 contact me [on Mastodon](https://mastodon.tetaneutral.net/@roipoussiere);
- 🗨 discuss [on the MastoZap group](https://framateam.org/mastozap/) on Framateam (Mattermost server provided by [Framasoft](https://framateam.org/mastozap/)).

## Coding

### 📦 Prerequies

You need node.js (>=8.10) and npm (>=5.6).

To install them on Debian-based systems:

```bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
echo -e "nodejs version:\t$(nodejs -v) \nnpm version:\t$(npm -v)"
# check that you have node.js >=8.10 npm >=5.6
```

Then install the Zapier CLI tool:

```bash
sudo npm install -g zapier-platform-cli
zapier -v
# check that you have zapier CLI tool
```

### 📚 Interesting readings

- To be familiar with Zapier app development, you can follow [the Zapier getting started guide](https://zapier.github.io/zapier-platform-cli);
- you can also take a look to [the Mastodon API](https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md);
- I also write some notes for developers on [the MastaZap wiki](https://framagit.org/roipoussiere/MastoZap/wikis/home).

### 🏗️ Installation

Here we go! 🤠 First, clone this repository:

```bash
git clone https://framagit.org/roipoussiere/MastoZap.git
```

Then install the app with npm:

```bash
npm install
```

And check that everything works by running the tests:

```bash
zapier test
```

Yay! You are ready! 🍾

### ⤴️ Creating a merge request

- 1. First, [create a fork of this project](https://framagit.org/roipoussiere/MastoZap/forks/new), and copy the https URL (under the badges) of your project (something like https://framagit.org/YOUR_USERNAME/MastoZap.git );
- 2.a If you already cloned and worked on the project: `git remote add upstream https://framagit.org/YOUR_USERNAME/MastoZap.git`;
- 2.b otherwise, clone your fork: `git clone https://framagit.org/YOUR_USERNAME/MastoZap.git` and commit your work;
- 3. test your work:
  - check files against Zapier syntax: `zapier validate --debug`;
  - run unit tests: `zapier test`;
  - check eslint errors and warnings: `./node_modules/.bin/eslint .`;
  - check code coverage: `./node_modules/.bin/nyc ./node_modules/.bin/mocha`;
- 4. push it to a dedicated branch `git push origin myMergeRequest`;
- 5. got to the [main project page](https://framagit.org/roipoussiere/MastoZap) and click on the button *Send merge request*, then fill the description.

Thank you! 💜
