#!/bin/bash

# Usage:
# gitlab-badge.sh <badge_pos> <badge_title> <badge_value> <badge_color> <badge_link>

# todo:
# - make the badge locally with https://github.com/badges/shields
# - change the color according to the result
# - make it usable by anyone easily in gitlab-ci.yml (dockerfile?)

api_endpoint="${CI_PROJECT_URL}/../../api/v4"
badge_api="${api_endpoint}/projects/${CI_PROJECT_ID}/badges"

py="import json,sys;obj=json.load(sys.stdin);print obj[$1-1]['id']"
badge_id=$(curl -sS --header "PRIVATE-TOKEN: ${BADGE_TOKEN}" "${badge_api}" | python -c "$py")

image_url="https://img.shields.io/badge/$2-$3-$4.svg"

echo "badge position: $1"
echo "badge id: ${badge_id}"
echo "new badge title: $2"
echo "new badge value: $3"
echo "new badge color: $4"
echo "new badge link:  $5"

curl -sS \
  --request PUT \
  --header "PRIVATE-TOKEN: ${BADGE_TOKEN}" \
  --data-urlencode "link_url=$5" \
  --data-urlencode "image_url=${image_url}" \
  "${badge_api}/${badge_id}"

