# MastoZap

__*Work in progress: this project doesn't work yet.*__

A [Zapier](https://zapier.com/) app for Mastodon alowing users to easily automate several tasks with their favorite social media.

The long-term idea is to write apps [for other federated projects](https://framagit.org/roipoussiere/MastoZap/wikis/home#apis-of-federated-projects) such as PeerTube or PixelFed.

Want to contribute? Thank! We have [a contribution guide](CONTRIBUTING.md) just for you!

## How it works?

The basic workflow of Zapier apps is *If this, then that*:

- several events on your Mastodon account can **trigger** many apps;
- many apps can execute **actions** on you Mastodon account.

📜 Some examples:

- **if** I publish some content on a Facebook page, **then** I send a toot on Mastodon*;
- **if** a bloger sends a content on a RSS feed, **then** I send a toot on Mastodon*;
- **if** I send a toot on Mastodon, **then** I send a tweet on Twitter;
- **if** I follow someone on Mastodon, **then** I follow someone on Mastodon (ie. for backup accounts).

__*__ Note that to send non-private toots, your account must be tagged as a bot. 🤖

This project is used to write the Zapier triggers and actions.

Based on them, you can easily write the user stories (*Zaps*) like below, or use user stories that are already available.

There are [a lot of apps](https://zapier.com/apps) available on Zapier, check it out!
