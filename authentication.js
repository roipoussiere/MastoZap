// TODO: check connexions during api calls (look for most common error codes)
// ie.: if (res.status !== 200) { throw new Error('Unable to fetch access token: ' + res.content); }

const authorizeUrl = async (z, bundle) => {
  // Fetch clientId and clientSecret
  const { clientId, clientSecret } = await z.request({
    method: 'POST',
    url: `https://${bundle.inputData.server_name}/api/v1/apps`,
    body: {
      client_name: 'Zapier',
      website: 'https://framagit.org/roipoussiere/MastoZap',
      scopes: `${bundle.inputData.scope}`,
      redirect_uris: `${bundle.inputData.redirect_uri}`, // Zapier generates it automatically
    },
  }).then(res => res.json);

  // Store client_id and client_secret
  await z.cursor.set(`${clientId}|${clientSecret}`);

  // Return the authorization URL
  return {
    method: 'GET',
    url: `https://${bundle.inputData.server_name}/oauth/authorize`,
    params: {
      client_id: `${bundle.inputData.client_id}`,
      scope: `${bundle.inputData.scope}`,
      redirect_uri: `${bundle.inputData.redirect_uri}`, // Zapier generates it automatically
      response_type: 'code',
    },
  };
};

const getAccessToken = async (z, bundle) => {
  // Retrieve clientId and clientSecret
  const [clientId, clientSecret] = await z.cursor.get().split('|');

  // Fetch access token
  const { accessToken } = await z.request({
    method: 'POST',
    url: `https://${bundle.inputData.server_name}/oauth/token`,
    body: {
      code: bundle.inputData.code,
      client_id: clientId,
      client_secret: clientSecret,
      redirect_uri: bundle.inputData.redirect_uri,
      grant_type: 'authorization_code',
    },
  }).then(res => res.json);

  // This is stored in `bundle.authData` for later usage
  return {
    client_id: clientId,
    client_secret: clientSecret,
    accessToken,
  };
};

const testAuth = (z, bundle) => {
  const promise = z.request({
    method: 'GET',
    url: `${bundle.inputData.server_name}/api/v1/accounts/verify_credentials`,
  });

  // Returns a truthy value to indicate the credentials are valid.
  return promise.then((response) => {
    if (response.status === 401) {
      throw new Error('The access token you supplied is not valid');
    }
    return z.JSON.parse(response.content);
  });
};

module.exports = {
  type: 'oauth2',
  oauth2Config: {
    authorizeUrl,
    getAccessToken,
    // This app scope will get passed along to the authorizeUrl
    // TODO: use this instead of hard-coding it on api calls
    scope: 'read',
  },

  fields: [
    {
      key: 'server_name', type: 'string', required: true, default: 'mastodon.social',
    },
    {
      key: 'client_id', type: 'string', required: true, computed: true,
    },
    {
      key: 'client_secret', type: 'string', required: true, computed: true,
    },
  ],

  // Execute by Zapier after the OAuth flow is complete to ensure everything is setup properly.
  test: testAuth,
  // key returned from the test
  connectionLabel: '{{username}}',
};
